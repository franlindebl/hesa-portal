import { HesaPortalPage } from './app.po';

describe('hesa-portal App', function() {
  let page: HesaPortalPage;

  beforeEach(() => {
    page = new HesaPortalPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
