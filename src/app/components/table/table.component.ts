import {Component} from "@angular/core";
import {Input} from "@angular/core";
import {Output} from "@angular/core";
import {EventEmitter} from "@angular/core";
import {HomeComponent} from "../../home/components/home.component"

@Component({
    selector: 'table-component',
    templateUrl: 'table.html'
})
export class TableComponent {
  private _data:any;
  private tableHeaders:Array<string>;
  @Input()
  get data(): any {
    return this._data;
  }

  set data(data: any) {
    console.log(data);
    this._data = data;
  }


  constructor( ) {
    console.log("DATA ->",this);
    //this.tableHeaders = Object.keys(this.data);
  }


}
