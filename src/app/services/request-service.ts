import {Injectable} from "@angular/core";
import { Inject } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import {Test} from "../models/home-item";
//import {Task} from "../models/home-item";
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';


@Injectable()
export class RequestService {

  private url = 'https://hesaplanesp1942412250trial.hanatrial.ondemand.com';
  private port = 443;
  private baseUri = '1.0.0';
  private apiUrl = this.url + ':' + this.port + '/' + this.baseUri + '/';
  private headers = new Headers({'Accept':'application/json'});
  private options = new RequestOptions({headers: this.headers});
  constructor(private http:Http) { }

  get(uri: string):Observable<Test> {
    return this.http.get( this.apiUrl + uri, this.options )
    //return this.http.get( 'https://raw.githubusercontent.com/opendatajson/football.json/master/2010-11/en.1.clubs.json')
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  post(uri: string, body: Object) {
    let bodyString = JSON.stringify(body);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });
    let options = new RequestOptions({
      headers: headers
    });

    return this.http.post( this.apiUrl + uri, body, options)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  put(uri: string, body: Object) {
    let bodyString = JSON.stringify(body);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });
    let options = new RequestOptions({
      headers: headers
    });

    return this.http.put( this.apiUrl + uri, body, options)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  delete(uri: string) {
    return this.http.delete( this.apiUrl + uri)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

}
