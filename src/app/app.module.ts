import {NgModule}      from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import { HttpModule } from '@angular/http';

import {AppComponent} from "./app.component";
import {HomeComponent} from "./home/components/home.component";

import {HeaderComponent} from "./components/header/header.component";
import {FooterComponent} from "./components/footer/footer.component";
import {TableComponent} from "./components/table/table.component";

import {routing, appRoutingProviders} from './app.routing';
import {RequestService} from './services/request-service';
import {FormsModule} from "@angular/forms";

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        routing,
        HttpModule
    ],
    declarations: [
        AppComponent,
        HomeComponent,
        HeaderComponent,
        FooterComponent,
        TableComponent
    ],
    providers: [
        appRoutingProviders,
        RequestService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
