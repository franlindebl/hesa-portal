import {Component} from "@angular/core";
import {Input} from "@angular/core";
import {RequestService} from "../../services/request-service";
import {TableComponent} from "../../components/table/table.component";

import {Output} from "@angular/core";
import {EventEmitter} from "@angular/core";
import {Observable} from 'rxjs/Rx';

@Component({
    selector: 'home',
    templateUrl: 'home.html'
})
export class HomeComponent {
  public test: Array<Object>;
  public name:string;
  public data: any = [{element:'test'}];


  constructor( private requestService: RequestService ) {
    this.test = [];
    this.name = "Liga ";
    this.data = [
        {
            "id": "H1231",
            "name": "Dia de San Patricio",
            "status": "PUBLICADO",
            "container": [
                {
                    "id": "12312",
                    "name": "Barril"
                },
                {
                    "id": "12311231",
                    "name": "Tercio"
                }
            ],
            "pointOfSales": [],
            "scheduledStartDate": "2017-02-08",
            "scheduledEndDate": "2017-02-08"
        },
        {
            "id": "H12312131",
            "name": "2x1 Heineken",
            "status": "GUARDADO",
            "container": [
                {
                    "id": "12312",
                    "name": "Barril"
                },
                {
                    "id": "12311231",
                    "name": "Tercio"
                }
            ],
            "pointOfSales": [],
            "scheduledStartDate": "2017-02-08",
            "scheduledEndDate": "2017-02-08"
        },
        {
            "id": "H7777",
            "name": "Champions League",
            "status": "PUBLICADO",
            "container": [
                {
                    "id": "12312",
                    "name": "Barril"
                },
                {
                    "id": "12311231",
                    "name": "Tercio"
                }
            ],
            "pointOfSales": [],
            "scheduledStartDate": "2017-02-08",
            "scheduledEndDate": "2017-02-08"
        }
    ];
    console.log(this.data);
  }

  ngOnInit() {
    this.requestService.get('plan').subscribe(
      data => {
        this.test = data.clubs;
        this.name = this.name + data.name;
        console.log(data);
        return true;
       },
       error => {
         console.error("Error saving food!");
         return Observable.throw(error);
       }
    );
      console.log("entra");
      console.log(this);
  }

}
